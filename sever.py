import socket
import threading
import sys
from random import randint 
import time

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
import os
import base64

class Server: 
    
    
        connections = [] #list of connectiosn
        peers = [] #list of peers
    
    
        def __init__(self):
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #initisle socket
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #allow sockets to be resued 
            sock.bind(("10.35.70.14", 33005)) #binding socket and port
            sock.listen(100) #listening for sonnections
            print("Server Started") #message so we know server is up
    
            while True:
                client_socket, client_addr = sock.accept()
                cThread = threading.Thread(target = self.handler, args=(client_socket, client_addr)) 
                #allows handler to run at saem tine
                cThread.daemon = True
                cThread.start() #starts threading process start used instead of run
                self.connections.append(client_socket)  #appends connectrions list
                self.peers.append(client_addr[0])    #appends peer lsit
                self.sendPeers()
                print(str(client_addr[0]) + ":" + str(client_addr[1]), "Connected!") #prints ip followed by port 



        def handler(self, client_socket, client_addr):
            while True:
                data = client_socket.recv(1024)
                key = data[:32] #newly added
                nonce = data[32:48]  #newly added
                message = data[48:] #newly added
                message = self.decrypt(message,nonce,key)
    
                newmessage = message + " - sent by " + str(client_addr[0]) + ":" + str(client_addr[1])
                print(newmessage)
    
                newmessage = self.encrypt(newmessage)
   
                #updatedData = str(client_addr[0]) + ":" + str(client_addr[1]) + " - " + str(data)
                #updatedData= encrypt(updatedData) 
                for connection in self.connections:
                    if connection != client_socket:
                            #connection.send(data)
                            connection.send(newmessage)
                if not data:
                    print(str(client_addr[0]) + ":" + str(client_addr[1]), "Disconnected!") 
                    #shows if client disocneects
                    self.connections.remove(client_socket)  #removes client from connections list
                    self.peers.remove(client_addr[0])  #removes peer from pees list
                    client_socket.close()
                    self.sendPeers()
                    break
        
            
        
        def sendPeers(self):
            p = ""
            for peer in self.peers:
                p = p + peer + ","
            for connection in self.connections:
                connection.send(b'\x11' + bytes(p, "utf-8"))
    
        def decrypt(self,ciphertext, nonce,key):
                    # Create a new ChaCha20 cipher with the shared key and the nonce

            cipher = Cipher(algorithms.ChaCha20(key, nonce), mode=None, backend=default_backend())
            decryptor = cipher.decryptor()
            ciphertext = base64.b64decode(ciphertext)      
        # Decrypt the ciphertext
            plaintext = decryptor.update(ciphertext) + decryptor.finalize()
            return plaintext.decode('ISO-8859-1')

        def encrypt(self, plaintext):
            key  = os.urandom(32) #newly added
            nonce = os.urandom(16)  #newly added
        # Create a new ChaCha20 cipher with the shared key and the nonce
            cipher = Cipher(algorithms.ChaCha20(key, nonce), mode=None, backend=default_backend())
            encryptor = cipher.encryptor()
            message = plaintext.encode()
        # Encrypt the plaintext
            ciphertext = encryptor.update(message) + encryptor.finalize()

        # Encode the ciphertext in base64 and return it as bytes
            return key+nonce+base64.b64encode(ciphertext)
        
  
class Client:
    def __init__(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)      
        sock.connect(("10.35.70.14", 33005))
        iThread = threading.Thread(target = self.sendMsg, args=(sock, ))  #allows sendMSG to run in background
        iThread.daemon = True
        iThread.start()
        print("Server: " + "Connected as Client.") #telles client they have been connected as a client

        while True:
            data = sock.recv(1024)
            if not data:
                break

   
            if data[0:1] == b'\x11':
                    print("Server: Someone Connected/Disconnected") #allows other clients to see if someone has joined or left
                    self.peersUpdated(data[1:])
                        
            else:
                key = data[:32] #newly added
                nonce = data[32:48]  #newly added
                data = data[48:] #newly added
                message = self.decrypt(data,nonce,key)
                print(message)

    
    def peersUpdated(self, peerData):
            p2p.peers = str(peerData, "utf-8").split(",")[:-1] #updates peeers
   

    def sendMsg(self, sock):
        while True:
                message = input("")
                message = self.encrypt(message) 
                sock.send(message) #simple funcion for sending messages
    
    def decrypt(self,ciphertext, nonce,key):
                # Create a new ChaCha20 cipher with the shared key and the nonce

        cipher = Cipher(algorithms.ChaCha20(key, nonce), mode=None, backend=default_backend())
        decryptor = cipher.decryptor()
        ciphertext = base64.b64decode(ciphertext)      
        # Decrypt the ciphertext
        plaintext = decryptor.update(ciphertext) + decryptor.finalize()
        return plaintext.decode('ISO-8859-1')

    def encrypt(self, plaintext):
        key  = os.urandom(32) #newly added
        nonce = os.urandom(16)  #newly added
        # Create a new ChaCha20 cipher with the shared key and the nonce
        cipher = Cipher(algorithms.ChaCha20(key, nonce), mode=None, backend=default_backend())
        encryptor = cipher.encryptor()
        message = plaintext.encode()
        # Encrypt the plaintext
        ciphertext = encryptor.update(message) + encryptor.finalize()

        # Encode the ciphertext in base64 and return it as bytes
        return key+nonce+base64.b64encode(ciphertext)

class p2p:
    peers = ["0.0.0.0"] #default peer
 
while True:
            try:
                print("Trying to connect ...")  #message to display if server goes down
                time.sleep(randint(1, 5))   #will make peers wait a random amount of time
                for peer in p2p.peers:
                    try:
                        client = Client()
                    except KeyboardInterrupt:
                        sys.exit(0)
                    except:
                        pass
                    if randint(1, 20) <= 21:
                        try:
                            server = Server()
                        except KeyboardInterrupt:
                            sys.exit(0)
                        except:
                            print("couldnt find server")
            except KeyboardInterrupt:
                    sys.exit(0)
