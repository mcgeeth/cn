import socket
import threading
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
import os
import base64

clients = []  # list of all connected clients

# The shared key for ChaCha20 encryption
key = os.urandom(32)
#nonce = os.urandom(16)


def handle_client(client_socket):
    # Add the new client to the clients list
    clients.append(client_socket)
    print("New client connected: ", client_socket)

    while True:
        message = client_socket.recv(1024)
        message = decrypt(message,nonce)
        print("Partner:" + message)

        # forward the received message to all connected clients except the sender
        for c in clients:
            if c != client_socket:
                c.send(encrypt(message, nonce).encode()) # encrypt the message before sending it to other clients
                break

def start_server():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(("10.35.70.14", 33005))
    server.listen()

    print("Server started, waiting for clients to connect...")

    while True:
        client_socket, _ = server.accept()
        clients.append(client_socket)
        print("New client connected: ", client_socket)

        # start a new thread to handle the new client
        threading.Thread(target=handle_client, args=(client_socket,)).start()

def connect_to_server():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(("10.35.70.14", 33005))

    threading.Thread(target=sending_messages, args=(client,)).start()
    while True:
        message = client.recv(1024)
        message = decrypt(message, nonce) # decrypt the message before printing it to the console
        print("Partner: " + message)

def sending_messages(c):
    while True:
        message = input("")
        print("YOU: " + message)
        message = encrypt(message, nonce) 
        c.send(message)
  

def decrypt(ciphertext, nonce):
    # Create a new ChaCha20 cipher with the shared key and the nonce
    cipher = Cipher(algorithms.ChaCha20(key, nonce), mode=None, backend=default_backend())
    decryptor = cipher.decryptor()

    print("3")
    print(ciphertext)
    # Decode the ciphertext from base64
    ciphertext = base64.b64decode(ciphertext)
    print("2 ")
    print(ciphertext)

    # Decrypt the ciphertext
    plaintext = decryptor.update(ciphertext) + decryptor.finalize()
    print("1 ")
    print(plaintext)

    return plaintext.decode('ISO-8859-1')


def encrypt(plaintext, nonce):
    # Create a new ChaCha20 cipher with the shared key and the nonce
    cipher = Cipher(algorithms.ChaCha20(key, nonce), mode=None, backend=default_backend())
    encryptor = cipher.encryptor()

    print("1 " + plaintext)
    # Encrypt the plaintext
    ciphertext = encryptor.update(plaintext.encode('ISO-8859-1')) + encryptor.finalize()
    print ("2 ")
    print(ciphertext)

    # Encode the ciphertext in base64 and return it as bytes
    #return base64.b64encode(ciphertext)
    ciphertext = base64.b64encode(ciphertext)
    print("3 ")
    print(ciphertext)
    return ciphertext

if __name__ == '__main__':
    # Generate a random nonce
    #nonce = os.urandom(16)
    nonce = b'b\xcf\x8f\xafA\x9a\x01\x02\x85^ds\xa7\xcc\xaes'
    print(nonce)

    choice = input("Do you want to host(1) or do you want to connect(2):")
    if choice == "1":
        start_server()

    elif choice == "2":
        connect_to_server()
    else:
        exit()
