import socket
import threading
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
import os
import base64

clients = []  # list of all connected clients

# The shared key for ChaCha20 encryption
#key = os.urandom(32)
#key = b'u3XZcaz\xd7\x9c0\\$>\x1e6?4\x18\x19(*7\x89\x04\x02W^\x052\xaa]\x03'
#nonce = b';\xb7\xe6\xea\xda\x81+\xe9\xfb\xab\xdf9\xee\x8ep\xe0'



def handle_client(client_socket):
    # Add the new client to the clients list
    clients.append(client_socket)
    print("New client connected: ", client_socket)

    while True:
        message = client_socket.recv(1024)
        
        key = message[:32] #newly added
        nonce = message[32:48]  #newly added
        message = message[48:] #newly added
        
        message = decrypt(message,nonce,key)
   
        print("Partner:" + message)
        # forward the received message to all connected clients except the sender
        for c in clients:
            if c != client_socket:
                message = encrypt(message)
                c.send(message ) # encrypt the message before sending it to other clients
                break

def start_server():
    global key, nonce
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(("10.35.70.14", 33005))
    server.listen()
    print("Server started, waiting for clients to connect...")

    while True:
        client_socket, _ = server.accept()
        clients.append(client_socket)
        print("New client connected: ", client_socket)
        # start a new thread to handle the new client
        threading.Thread(target=handle_client, args=(client_socket,)).start()
        

def connect_to_server():   
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(("10.35.70.14", 33005))

    threading.Thread(target=sending_messages, args=(client,)).start()
    while True:
        message = client.recv(1024)
        
        key = message[:32]
        nonce = message[32:48]  #newly added
        message = message[48:] #newly added
        
        message = decrypt(message, nonce, key) # decrypt the message before printing it to the console
        print("Partner: " + message)

def sending_messages(c):
    while True:
        message = input("")
        print("YOU: " + message)
        message = encrypt(message) 
        print("No 1")
        print(message)
        c.send(message)
  

def decrypt(ciphertext, nonce,key):
    # Create a new ChaCha20 cipher with the shared key and the nonce
    cipher = Cipher(algorithms.ChaCha20(key, nonce), mode=None, backend=default_backend())
    decryptor = cipher.decryptor()
    
    print("key and nonce")
    print(key)
    print(nonce)
    
    ciphertext = base64.b64decode(ciphertext)
    
    

    # Decrypt the ciphertext
    plaintext = decryptor.update(ciphertext) + decryptor.finalize()


    return plaintext.decode('ISO-8859-1')


def encrypt(plaintext):
    key  = os.urandom(32) #newly added
    nonce = os.urandom(16)  #newly added
    # Create a new ChaCha20 cipher with the shared key and the nonce
    cipher = Cipher(algorithms.ChaCha20(key, nonce), mode=None, backend=default_backend())
    encryptor = cipher.encryptor()
    print("key and nonce")
    print(key)
    print(nonce)

    message = plaintext.encode()


    # Encrypt the plaintext
    ciphertext = encryptor.update(message) + encryptor.finalize()
    


    # Encode the ciphertext in base64 and return it as bytes
    return key+nonce+base64.b64encode(ciphertext)

if __name__ == '__main__':


    choice = input("Do you want to host(1) or do you want to connect(2):")
    if choice == "1":
        start_server()

    elif choice == "2":
        connect_to_server()
    else:
        exit()
